# Colors
Simple llibreria de Colors per utilitzar-la en els programes consola
Implementada en Java8

## Colors

- [ ] cyan 
- [ ] verd 
- [ ] blau 
- [ ] negre
- [ ] marró
- [ ] magenta
- [ ] gris
- [ ] colorNormal



## Tipografia

- [ ] Normal
- [ ] Negreta
- [ ] Cursiva
- [ ] Subratllat
- [ ] Ratllat

## Usabilitat
Importa la llibreria. Crea una variable de la classe i ja pots utilitzar-la.
Per exemple:
```
Colors cl=new Colors;
System.out.println(cl.roig+"Hola, com va?"+cl.nc);
```

## Contribució
Si vols contribuir, si us plau, fes un *fork* del repositori i envia una sol·licitud de *pull request* amb les teves millores.

## Llicència
Aquest projecte està llicenciat sota la [Llicència MIT](LICENSE).
